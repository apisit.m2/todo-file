package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

func printItem(todoItems []string) {
	fmt.Println("------ Todo List ------")

	for i, v := range todoItems {
		fmt.Println(i+1, ":", v)
		fmt.Println("-----------------------")
	}
}

func formatInput(todoItem string) string {
	todoItem = strings.Trim(todoItem, "\n")
	todoItem = strings.TrimSpace(todoItem)
	return todoItem
}

func readFromTextFile() []string {
	if _, err := os.Stat("todos.txt"); errors.Is(err, os.ErrNotExist) {
		os.Create("todos.txt")
	}
	// open input file
	fi, err := os.Open("todos.txt")
	if err != nil {
		fmt.Println("Cannot open file")
		panic(err)
	}
	todoTemp := []string{}
	scanner := bufio.NewScanner(fi)
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		todoTemp = append(todoTemp, scanner.Text())
	}

	// close fi on exit and check for its returned error
	defer func() {
		if err := fi.Close(); err != nil {
			panic(err)
		}
	}()

	return todoTemp
}

func writeTextFile(todoItems string) {
	f, err := os.Create("./todos.txt")
	if err != nil {
		fmt.Println("Cannot create file")
		panic(err)
	}

	n3, err := f.WriteString(todoItems)
	if err != nil {
		fmt.Println("Cannot open file")
		panic(err)
	}

	fmt.Printf("wrote %d bytes\n", n3)
	f.Sync()
}

func readFromJsonFile() []string {
	// if _, err := os.Stat("todos.json"); errors.Is(err, os.ErrNotExist) {
	// 	_, err := os.Create("todos.json")
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// }

	todos := []Todos{}
	jsonFile, err := os.Open("todos.json")
	if errors.Is(err, os.ErrNotExist) {
		_, err := os.Create("todos.json")
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}

	// if err != nil {
	// 	panic(err)
	// }

	RawJson, err := io.ReadAll(jsonFile)
	if err != nil {
		panic(err)
	}

	if err := json.Unmarshal(RawJson, &todos); err != nil {
		panic(err)
	}

	var todosTemp []string

	for _, todo := range todos {
		todosTemp = append(todosTemp, todo.Name)
	}

	return todosTemp
}

var format = flag.String("format", "text", "")

func main() {
	flag.Parse()

	var todoItems []string

	if *format == "json" {
		todoItems = readFromJsonFile()
	} else {
		todoItems = readFromTextFile()
		// var newInput string = ""
	}

	for {
		fmt.Print("input your new todo: ")

		in := bufio.NewReader(os.Stdin)
		input, err := in.ReadString('\n')
		input = formatInput(input)
		if err != nil {
			panic(err)
		} else if len(input) == 0 {
			// in.Reset(os.Stdin)
			printItem(todoItems)
			continue
		}

		// Todo if else text json
		todoItems = append(todoItems, input)
		if *format == "json" {
			writeFromJson(todoItems)
		} else {
			writeTextFile(strings.Join(todoItems, "\n"))
		}

		printItem(todoItems)
	}

}

func writeFromJson(todos []string) {
	todoJson := []Todos{}
	for _, todo := range todos {
		todoJson = append(todoJson, Todos{
			Name:   todo,
			Status: "default",
		})
	}
	f, err := os.Create("./todos.json")
	if err != nil {
		fmt.Println("Cannot create file")
		panic(err)
	}
	rawJson, err := json.Marshal(todoJson)
	if err != nil {
		panic(err)
	}
	n3, err := f.Write(rawJson)
	if err != nil {
		fmt.Println("Cannot write file")
		panic(err)
	}

	fmt.Printf("wrote %d bytes\n", n3)
	f.Sync()

}
